.class final Lname/ratson/cordova/admob/Actions;
.super Ljava/lang/Object;
.source "Actions.java"


# static fields
.field static final CREATE_BANNER:Ljava/lang/String; = "createBannerView"

.field static final CREATE_INTERSTITIAL:Ljava/lang/String; = "createInterstitialView"

.field static final CREATE_REWARD_VIDEO:Ljava/lang/String; = "createRewardVideo"

.field static final DESTROY_BANNER:Ljava/lang/String; = "destroyBannerView"

.field static final IS_INTERSTITIAL_READY:Ljava/lang/String; = "isInterstitialReady"

.field static final IS_REWARD_VIDEO_READY:Ljava/lang/String; = "isRewardVideoReady"

.field static final PREPARE_INTERSTITIAL:Ljava/lang/String; = "prepareInterstitial"

.field static final REQUEST_AD:Ljava/lang/String; = "requestAd"

.field static final REQUEST_INTERSTITIAL:Ljava/lang/String; = "requestInterstitialAd"

.field static final SET_OPTIONS:Ljava/lang/String; = "setOptions"

.field static final SHOW_AD:Ljava/lang/String; = "showAd"

.field static final SHOW_INTERSTITIAL:Ljava/lang/String; = "showInterstitialAd"

.field static final SHOW_REWARD_VIDEO:Ljava/lang/String; = "showRewardVideo"


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
