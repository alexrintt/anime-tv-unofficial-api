.class public final Lcom/achanceapps/tv/aaprojv2/R$attr;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/achanceapps/tv/aaprojv2/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "attr"
.end annotation


# static fields
.field public static final adSize:I = 0x7f010000

.field public static final adSizes:I = 0x7f010001

.field public static final adUnitId:I = 0x7f010002

.field public static final ad_marker_color:I = 0x7f010003

.field public static final ad_marker_width:I = 0x7f010004

.field public static final auto_show:I = 0x7f010005

.field public static final bar_height:I = 0x7f010006

.field public static final buffered_color:I = 0x7f010007

.field public static final buttonSize:I = 0x7f010008

.field public static final circleCrop:I = 0x7f010009

.field public static final colorScheme:I = 0x7f01000a

.field public static final controller_layout_id:I = 0x7f01000b

.field public static final default_artwork:I = 0x7f01000c

.field public static final fastforward_increment:I = 0x7f01000d

.field public static final font:I = 0x7f01000e

.field public static final fontProviderAuthority:I = 0x7f01000f

.field public static final fontProviderCerts:I = 0x7f010010

.field public static final fontProviderFetchStrategy:I = 0x7f010011

.field public static final fontProviderFetchTimeout:I = 0x7f010012

.field public static final fontProviderPackage:I = 0x7f010013

.field public static final fontProviderQuery:I = 0x7f010014

.field public static final fontStyle:I = 0x7f010015

.field public static final fontWeight:I = 0x7f010016

.field public static final hide_during_ads:I = 0x7f010017

.field public static final hide_on_touch:I = 0x7f010018

.field public static final imageAspectRatio:I = 0x7f010019

.field public static final imageAspectRatioAdjust:I = 0x7f01001a

.field public static final played_ad_marker_color:I = 0x7f01001b

.field public static final played_color:I = 0x7f01001c

.field public static final player_layout_id:I = 0x7f01001d

.field public static final repeat_toggle_modes:I = 0x7f01001e

.field public static final resize_mode:I = 0x7f01001f

.field public static final rewind_increment:I = 0x7f010020

.field public static final scopeUris:I = 0x7f010021

.field public static final scrubber_color:I = 0x7f010022

.field public static final scrubber_disabled_size:I = 0x7f010023

.field public static final scrubber_dragged_size:I = 0x7f010024

.field public static final scrubber_drawable:I = 0x7f010025

.field public static final scrubber_enabled_size:I = 0x7f010026

.field public static final show_shuffle_button:I = 0x7f010027

.field public static final show_timeout:I = 0x7f010028

.field public static final shutter_background_color:I = 0x7f010029

.field public static final surface_type:I = 0x7f01002a

.field public static final touch_target_height:I = 0x7f01002b

.field public static final unplayed_color:I = 0x7f01002c

.field public static final use_artwork:I = 0x7f01002d

.field public static final use_controller:I = 0x7f01002e


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
